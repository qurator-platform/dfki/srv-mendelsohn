# This file is a template, and might need editing before it works on your project.
FROM python:3.6


WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app
ADD https://drive.google.com/open?id=1xQbmSbA9b3h-3scmNRpQJWgk_lOX5Qli /usr/src/app
ADD https://drive.google.com/open?id=1Uy1evvD6yV4mELfhfbUhnXlN9rbzWEf0 /usr/src/app

# For Django
#EXPOSE 8000
CMD ["python", "mendelsohn_nlp/preproc_medelsohnTestFile.py"]


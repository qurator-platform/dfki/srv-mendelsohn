# srv-mendelsohn

## Requirements

### Installation

Install spaCy language packages: 
```
python -m spacy download en 
python -m spacy download de
```


Install topic-detection module from https://gitlab.com/qurator-platform/dfki/srv-topic-detection?nav_source=navbar as Python module:
 
```
git clone https://gitlab.com/qurator-platform/dfki/srv-topic-detection.git
cd srv-topic-detection
pip install .
./download_models.sh
```
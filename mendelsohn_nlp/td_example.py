from topic_detection.language import Language
from topic_detection.wikipedia import WikipediaTopics
from topic_detection.corpus import Corpus, LANGUAGES
from topic_detection.lda import get_topics

import pandas as pd


# Load Wikipedia embeddings (run download_models.sh before)
wt = WikipediaTopics.load('../models/simplewiki.pkl')

wt.most_similar_by_entity_vector(wt.get_word_vector('duck'))

wt.most_similar_by_entity_vector(wt.get_word_vector('dog'))

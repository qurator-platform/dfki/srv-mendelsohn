import pandas as pd
import spacy
from spacy.lang.de import German
nlp = spacy.load("de")
parser = German()

from stop_words import get_stop_words
stop_words = get_stop_words('de')

from mendelsohn_nlp.germalemma import GermaLemma
import pickle
import nltk
from langdetect import detect
from googletrans import Translator
translator = Translator()
import numpy as np



def shape():
    df = pd.read_csv('/Users/karolinazaczynska/Documents/DFKI/PyProjects/mendelsohn/data/mendelsohnDataset.csv', delimiter=';', encoding='utf-8', index_col=[0], parse_dates=True)
    df = df.drop(columns = [ 'pseudonym', 'ereignis_SWD', 'datum_vom', 'werk_SWD', 'ort', 'ort_geonames'])
    print(df.shape)
    df = df.rename(columns={"handschrift": "brief"})
    df = df.drop_duplicates()
    df.brief = df.brief[pd.notnull(df.brief)]

    df = df.dropna(subset=['brief'])
    #df = df.drop_duplicates(subset=['brief'])
    # index would cause error in tagger_f(): reset index
    df = df.reset_index(drop=True)
    print(df.shape)
    df.to_pickle('../data/mendelsohnDatasetShaped.pkl')
    df.to_csv('../data/mendelsohnDatasetShaped.csv', index=False)



from translate import Translator
import pydeepl

from textblob import TextBlob
import urllib.request as urllib
import time


def translate(data):
    data = data.replace(np.nan, '', regex=True)
    print(data.shape)

    data = data.iloc[24100:24110, :]
    data['language'] = data['brief'].apply(detect)
    data = data[data['language'].str.contains("de|en")]
    data = data.reset_index(drop=True)
    print(data)

    #en_blob = TextBlob(data['brief'][1])
    #print(en_blob.translate(from_lang='en', to='de'))
    #print(data['brief'][1])

    #df['brief_de'] = df['brief'].apply(lambda x: TextBlob(x).translate(to='de'))
    def trans(text):
        try:
            return TextBlob(text).translate(to='de')
        except urllib.HTTPError as e:
            if e.code == 429:
                time.sleep(5)
                return trans(text)
            raise

    df['brief_de'] = df['brief'].apply(trans)

    '''
    # library: py translator -> only 800 chars
    
    sentence = 'I like turtles.'
    from_language = 'EN'
    to_language = 'ES'

    translation = pydeepl.translate(sentence, to_language, from_lang=from_language)
    print(translation)

    # Using auto-detection
    translation = pydeepl.translate(sentence, to_language)
    print(translation)
    
    
    translator = Translator(to_lang='fr')
    translation = translator.translate(data['brief'][53])
    print(translation)
    print(len(data['brief'][53]))
    
    
    for i in range(len(data)):
        if data['language'][i] == 'en':
            data["brief_de"] = data["brief"].map(lambda x: translator.translate(x))
        elif data['language'][i] != 'en':
            data["brief_de"] = data["brief"]
        print(data["brief_de"][i])
    #data.to_csv('../data/test.csv')
    '''


    """
    #library googletrans -> api limitations
    data["brief_de"] = ""
    for index, row in data.iterrows():
        eng_text = translator.translate(row["brief"], src="en", dest="de").text
        row['brief_de'] = eng_text
        print(row['brief_de'])
    #letzter stand

    for i in range(len(data)):
        if data['language'][i] == 'en':
            data["brief_de"] = data["brief"].map(lambda x: translator.translate(x, src="en", dest="de").text)
        elif data['language'][i] != 'en':
            data["brief_de"] = data["brief"]
            #data["brief_de"][i] = data["brief"][i].map(lambda x: translator.translate(x, src="en", dest="de").text)
            #print(data[i])
    """








    #print(data.shape)
    #data.to_pickle('../data/mendelsohnDataset_ger.pkl')




def preproc_nlp(df):
    # df = df.rename(columns={"index": "index_handschrift"})
    # remove punctuation
    df.dropna(axis=0, subset=["brief"])
    df = df.reset_index()
    df['brief_tokens'] = df["brief"].str.replace('[^\w\s]', '')
    # tokenize
    df['brief_tokens'] = [nltk.word_tokenize(str(sentence)) for sentence in df['brief_tokens']]

    # apply pre-trained pos-tagger
    with open('../mendelsohn_nlp/german_postagger_model.pickle', 'rb') as f:
        tagger = pickle.load(f)

        def tagger_f(text):
            return tagger.tag(text)
        df['brief_pos'] = df['brief_tokens'].apply(tagger_f)

    # lemmatizer germalemma
    lemmatizer = GermaLemma()

    big_array = []
    for i in range(len(df['brief_pos'])):
        temp_str = ""
        for v in df['brief_pos'][i]:
            try:
                temp_str = temp_str + lemmatizer.find_lemma(v[0], v[1]) + " "
            except:
                temp_str = temp_str + v[0] + " "
        big_array.append(temp_str)

    df['brief_lemma'] = big_array
    df['brief_lemma'] = [token.lower() for token in df['brief_lemma']]
    df['brief_lemma'] = [nltk.word_tokenize(sentence) for sentence in df['brief_lemma']]

    pd.to_pickle(df, '../data/mendelsohnDataset_preproc.pkl')


#def extract_luise_erich():

"""

def lda_experiments(data):
    text_data = df['text_lemma']
    print(type(text_data[0]))
    # module implements the concept of a Dictionary – a mapping between words and their integer ids.
    dictionary = corpora.Dictionary(text_data)
    # doc2bow : Convert document into the bag-of-words (BoW) format = list of (token_id, token_count) tuples.
    corpus = [dictionary.doc2bow(text) for text in text_data]
    # pickle corpus & dic
    pickle.dump(corpus, open('corpus.pkl', 'wb'))
    dictionary.save('dictionary.gensim')

    NUM_TOPICS = 5
    ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=NUM_TOPICS, id2word=dictionary, passes=15)
    ldamodel.save('model5.gensim')
    topics = ldamodel.print_topics(num_words=4)
    for topic in topics:
        print(topic)
    # Visualizing 5 topics:
    dictionary = gensim.corpora.Dictionary.load('dictionary.gensim')
    corpus = pickle.load(open('corpus.pkl', 'rb'))
    lda = gensim.models.ldamodel.LdaModel.load('model5.gensim')

    lda_display = pyLDAvis.gensim.prepare(lda, corpus, dictionary, sort_topics=False)
    pyLDAvis.display(lda_display)





    for text in df['handschrift']:
        langid.apply(detect(text))
        print("Percent of data in German (estimated):")
        print((sum(langid == "de") / len(langid)) * 100)
        print("Percent of data in English (estimated):")
        print((sum(langid == "fr") / len(langid)) * 100)
        print("Percent of data in French (estimated):")
        print((sum(langid == "en") / len(langid)) * 100)

    # convert list of languages to a dataframe
    langs_df = pd.DataFrame(langid)
    # count the number of times we see each language
    langs_count = langs_df.Tweet.value_counts()
    # horrible-looking barplot (I would suggest using R for visualization)
    langs_count.plot.bar(figsize=(20, 10), fontsize=20)


"""
if __name__ == "__main__":
    #shape()
    df = pd.read_pickle('../data/mendelsohnDatasetShaped.pkl')
    #translate(df)
    #df = pd.read_pickle('../data/mendelsohnDataset_ger.pkl')
    #preproc_nlp(df)
    #df = pd.read_pickle('../data/mendelsohnDataset_ger_preproc.pkl')


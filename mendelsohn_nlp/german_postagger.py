"""Train POS-Tagger on TIGER corpus and pickle learning model"""

from nltk.corpus.reader.conll import ConllCorpusReader
from mendelsohn_nlp.classifier_based_german_tagger import ClassifierBasedGermanTagger
import random
import pickle

# Obtain & load TIGER training corpus
file_path = '/Users/karolinazaczynska/Documents/DFKI/PyProjects/mendelsohn/data/'
corp = ConllCorpusReader(file_path, 'tiger_release_aug07.corrected.16012013.conll09', ['ignore', 'words', 'ignore', 'ignore', 'pos'], encoding='utf-8')


# load sentences from corpus and split them for training and evaluation
tagged_sents = list(corp.tagged_sents())
random.shuffle(tagged_sents)
split_perc = 0.1
split_size = int(len(tagged_sents) * split_perc)
train_sents, test_sents = tagged_sents[split_size:], tagged_sents[:split_size]


# Using tagger and training it
tagger = ClassifierBasedGermanTagger(train=train_sents)
accuracy = tagger.evaluate(test_sents)
print(tagger.tag(['Das', 'ist', 'ein', 'einfacher', 'Test']))
print(accuracy)
# accuracy: around 0.94

# Saving trained pos-tagger with pickle
with open('german_postagger_model.pickle', 'wb') as f:
    pickle.dump(tagger, f, protocol=2)




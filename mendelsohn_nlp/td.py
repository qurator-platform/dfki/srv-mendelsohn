from topic_detection.language import Language
from topic_detection.wikipedia import WikipediaTopics
from topic_detection.corpus import Corpus, LANGUAGES
from topic_detection.lda import get_topics

import pandas as pd

# Load Wikipedia embeddings (run download_models.sh before)
wt = WikipediaTopics.load('../models/dewiki_20180420_100d.pkl')

# New corpus named `foo` in english language and 10 topics
lang = LANGUAGES['de']
c = Corpus('mendelsohn', lang, num_topics=10)

# Each doc is represented as str docs = ['foo bar. bar foo!', 'this is a test. foo.']

df = pd.read_pickle('../data/mendelsohnDataset_preproc.pkl')
docs = df['brief_lemma'].apply(' '.join)
docs = docs.drop_duplicates()
docs = docs.dropna()
docs = docs[pd.notnull(docs)]



# Build corpus + LDA model + find matching wiki pages
c.train(docs, wikipedia_topics=wt)

# Trained topics
#print(f'Available topics: {c.topics}')

# Assign topics to a new document
for v in docs[:50]:
    print(get_topics(v, c, num_topics=5))

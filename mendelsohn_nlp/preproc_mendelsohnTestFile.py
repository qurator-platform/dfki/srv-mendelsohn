import pandas as pd
import spacy
from spacy.lang.de import German

nlp = spacy.load("de")
parser = German()

from stop_words import get_stop_words

stop_words = get_stop_words('de')


from germalemma import GermaLemma

import pickle
import nltk
from googletrans import Translator

translator = Translator()


def shape():
    df = pd.read_csv('/Users/karolinazaczynska/Documents/DFKI/PyProjects/mendelsohn/data/mendelsohnDataset.csv',
                     delimiter=';', encoding='utf-8', index_col=[0], parse_dates=True)
    df = df.drop(columns=['pseudonym', 'ereignis_SWD', 'datum_vom', 'werk_SWD', 'ort', 'ort_geonames'])
    df = df.rename(columns={"handschrift": "brief"})
    df.brief = df.brief[pd.notnull(df.brief)]
    df = df.dropna(subset=['brief'])
    df = df.drop_duplicates(subset=['brief'])

    # index would cause error in tagger_f(): reset index
    df = df.reset_index(drop=True)
    df=df.iloc[:100,:]
    print(df.brief)
    df.to_pickle('../data/mendelsohnDatasetShapedTEST.pkl')
    #df.to_csv('../data/mendelsohnDatasetShaped.csv', index=False)






def preproc_nlp(df):
    # remove punctuation
    df['brief_tokens'] = df["brief"].str.replace('[^\w\s]', '')
    # tokenize
    df['brief_tokens'] = [nltk.word_tokenize(str(sentence)) for sentence in df['brief_tokens']]

    # apply pre-trained pos-tagger
    with open('../mendelsohn_nlp/german_postagger_model.pickle', 'rb') as f:
        tagger = pickle.load(f)

        def tagger_f(text):
            return tagger.tag(text)

        df['brief_pos'] = df['brief_tokens'].apply(tagger_f)

    # lemmatizer germalemma
    lemmatizer = GermaLemma()

    big_array = []
    for i in range(len(df['brief_pos'])):
        temp_str = ""
        for v in df['brief_pos'][i]:
            try:
                temp_str = temp_str + lemmatizer.find_lemma(v[0], v[1]) + " "
            except:
                temp_str = temp_str + v[0] + " "
        big_array.append(temp_str)

    df['brief_lemma'] = big_array
    df['brief_lemma'] = [token.lower() for token in df['brief_lemma']]
    df['brief_lemma'] = [nltk.word_tokenize(sentence) for sentence in df['brief_lemma']]
    print(df.brief_lemma)
    pd.to_pickle(df, '../data/mendelsohnDataset_preprocTEST.pkl')


import gensim
import gensim.corpora as corpora


def lda_experiments(df):
    text_data = df['brief_lemma']
    print(type(text_data[0]))
    # module implements the concept of a Dictionary – a mapping between words and their integer ids.
    dictionary = corpora.Dictionary(text_data)
    # doc2bow : Convert document into the bag-of-words (BoW) format = list of (token_id, token_count) tuples.
    corpus = [dictionary.doc2bow(text) for text in text_data]
    # pickle corpus & dic
    pickle.dump(corpus, open('corpusTEST.pkl', 'wb'))
    dictionary.save('dictionaryTEST.gensim')

    NUM_TOPICS = 5
    ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=NUM_TOPICS, id2word=dictionary, passes=15)
    ldamodel.save('model5TEST.gensim')
    topics = ldamodel.print_topics(num_words=4)
    for topic in topics:
        print(topic)


if __name__ == "__main__":
    shape()
    df = pd.read_pickle('../data/mendelsohnDatasetShapedTEST.pkl')
    preproc_nlp(df)
    df = pd.read_pickle('../data/mendelsohnDataset_preprocTEST.pkl')
    lda_experiments(df)

